const map = [
    "WWWWWWWWWWWWWWWWWWWWW", //15 rows, 21 elements in each row
    "W   W     W     W W W",
    "W W W WWW WWWWW W W W",
    "W W W   W     W W   W",
    "W WWWWWWW W WWW W W W",
    "W         W     W W W",
    "W WWW WWWWW WWWWW W W",
    "W W   W   W W     W W",
    "W WWWWW W W W WWW W X",
    "S     W W W W W W WWW",
    "WWWWW W W W W W W W W",
    "W     W W W   W W W W",
    "W WWWWWWW WWWWW W W W",
    "W       W       W   W",
    "WWWWWWWWWWWWWWWWWWWWW"
];
let betterMap = [];
betterMap = map.map((e) => e.replace(/\s/g, 'F').split('')); //'F' indicates 'Free' space to move; Reg exp replaces 'spaces' with 'F value in array
let board = document.querySelector('main');
let player = { x: 0, y: 0 }; //starts player object with x value of 0, y value of 0

for (let i = 0; i < betterMap.length; i++) {

    let rowElement = document.createElement("div");
    rowElement.classList.add('row');
    rowElement.dataset.x = i; //setting dataset of each row to i (row 1, row 2, etc)
    board.appendChild(rowElement);

    for (let j = 0; j < betterMap[i].length; j++) {

        let cellType = betterMap[i][j]; //cellType will be array element value ('S', 'F', 'W', or 'X')
        let cellElement = document.createElement("div");

        cellElement.classList.add("cell", cellType);
        cellElement.dataset.x = i; //setting data value of each cell (x value of i, y value of j --cell row1cell1, row1cell2, etc.)
        cellElement.dataset.y = j;
        cellElement.dataset.type = betterMap[i][j]; //data 'type' set to value of cell ('S', 'F', 'W', or 'X')
        cellElement.id = `x${i},y${j}` //id='x1y1', 'x1y2', x1y3,

        rowElement.appendChild(cellElement);

        if (cellType === 'S') { //if cellType is at Start, player x value set to i, y value set to j
            player.x = i; //empty 'player object' x-value set to i
            player.y = j; //empty 'player object' y-value set to j
        }
    }

}

document.addEventListener('keydown', moveSwitch); //on keydown move (right, left, up, down), call moveSwitch

function moveSwitch(keyName) {
    console.log(keyName.key); //keyName.key = 'ArrowRight','ArrowUp', etc..
    let cLoc = document.getElementById(`x${player.x},y${player.y}`) //grabs starting player position (player.x = i, player.y = j from 'S' location)
    let nLoc = '' //empty string; will be switched to new location depending on keyArrow switch
    if (keyName.key === "ArrowRight") nLoc = document.getElementById(`x${player.x},y${player.y + 1}`)
    if (keyName.key === "ArrowLeft") nLoc = document.getElementById(`x${player.x},y${player.y - 1}`)
    if (keyName.key === "ArrowUp") nLoc = document.getElementById(`x${player.x - 1},y${player.y}`)
    if (keyName.key === "ArrowDown") nLoc = document.getElementById(`x${player.x + 1},y${player.y}`)


    switch (keyName.key) {

        case arrow = "ArrowRight": //if keyName = ArrowRight; set new location(will be current location) + current Location(will be old location), and add 1 to y value type.
            if (nLoc.dataset.type === "F") {
                nLoc.dataset.type = "S" //setting new Location (which is now current location) type to 'start'(green)
                cLoc.dataset.type = "F" //Current Location becomes old location; set it back to 'Free' (white)
                player.y += 1; //moving right increases y value by 1
            }
            if (nLoc.dataset.type === "X") { //'X' type is the 'Finish' cell
                alert("You found your way out of the maze!")
            }
            break;

        case arrow = "ArrowLeft":
            if (nLoc.dataset.type === "F") {
                nLoc.dataset.type = "S" // sets new Location to 'Start' (green cell)
                cLoc.dataset.type = "F" //sets current Location to "Free" (white cell)
                player.y -= 1 //moving left subtracts y-value by 1;
            }
            break;

        case arrow = "ArrowUp":
            if (nLoc.dataset.type === "F") {
                nLoc.dataset.type = "S"
                cLoc.dataset.type = "F"
                player.x -= 1 //moving 'up' subtracts 1 to x; x value is the rows
            }
            break;
        case arrow = "ArrowDown":
            if (nLoc.dataset.type === "F") {
                nLoc.dataset.type = "S"
                cLoc.dataset.type = "F"
                player.x += 1 //moving 'down' adds 1 to x value;
            }
            break;
    }
}